let AWS = require('aws-sdk'),
    fs = require('fs'),
    request = require('request').defaults({
        encoding: null
    }),
    rp = require('request-promise').defaults({
        encoding: null
    }),
    sharp = require('sharp'),
    path = require('path'),
    config = require('config'),
    dbConfig = config.get('database'),
    mysql = require('mysql'),
    async = require('async');

//Set region
AWS.config.update({
    region: 'us-east-1'
});

// Create S3 service object
let s3 = new AWS.S3({
    apiVersion: '2006-03-01'
});

let uploadParams = {
    Bucket: 'agrinamics-images',
    Key: '',
    Body: ''
};

// handle 20 images at a time
let q = async.queue(function(params, callback) {

    // use promise to wait for atomic transaction of image
    // download, processing, and upload to complete before
    // marking the action as "done" by invoking the callback
    handleImage(params.uri, params.file).then(callback);

}, 20);

// drain is triggered when all queued actions have completed
let startTime = Date.now();
q.drain = function() {
    console.log('all items have been processed in ' + (Date.now() - startTime) + 'ms');
};

// download file, scale it, and write it to S3
async function handleImage(uri, file) {
    let t0 = Date.now();
    console.log('awaiting download: ' + file);
    let buffer = await download(uri, file);
    console.log('awaiting transform: ' + file);
    buffer = await transform(buffer, file);
    console.log('awaiting upload: ' + file);
    await upload(uploadParams, buffer, file);
    let t1 = Date.now();
    console.log('ending image:' + file + '(' + (t1 - t0) + 'ms)');
}

async function download(uri, filename) {
    return new Promise((resolve, reject) => {
        request.head(uri, function(err, res, body) {
            if (err) {
                console.log("Error", err);
                reject();
            } else {
                console.log('content-type:', res.headers['content-type']);
                console.log('content-length:', res.headers['content-length']);

                rp({
                    url: uri,
                    method: 'GET'
                }).then(async function(body) {
                    //console.log('awaiting transform: ' + filename);
                    //await transform(Buffer.from(body), filename);
                    resolve(Buffer.from(body));
                }).catch(function(err) {
                    reject();
                    console.log(err);
                });
            }
        });
    });
};

// use the sharp library to resize the image
async function transform(buffer, filename) {
    return new Promise((resolve, reject) => {
        //the sharp.toFile() function returns a Promise if no callback is provided
        sharp(buffer).resize(350, 350).toBuffer(async function(err, outputBuffer) {
            if (err) {
                reject();
                throw err;
            } else {
                //console.log('awaiting upload: ' + filename);
                //await upload(uploadParams, outputBuffer, filename);
                resolve(outputBuffer);
            }
        });
    });
};


// upload the resized file to S3
async function upload(params, outputBuffer, filename) {
    return new Promise((resolve, reject) => {

        uploadParams.Body = outputBuffer;
        uploadParams.Key = filename;

        // call S3 to retrieve upload file to specified bucket
        s3.upload(uploadParams, function(err, data) {
            if (err) {
                console.log("Error", err);
                reject();
            }
            if (data) {
                console.log("Upload Success", data.Location);
                resolve();
            }
        });
    });
};

function run(callback) {
    console.time("execution");

    const query = "SELECT id, imported_id, name, productMediumImage FROM products LIMIT 100";
    const dbConn = mysql.createConnection(dbConfig);

    dbConn.connect(function(err) {
        if (err) throw err;
    });

    dbConn.query(query, function(err, result, fields) {
        if (err) throw err;

        result.forEach(function(row) {
            var filename = row.id + '.jpg';
            q.push({
                uri: row.productMediumImage,
                file: filename
            }, function(err) {
                if (err) {
                    return console.log('error in adding tasks to queue');
                }
            });
        });
        console.log('resulted');
        console.timeEnd("execution");

    });
    dbConn.end(); // sdd - must terminate connection for node to stop running
};

run();